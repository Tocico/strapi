import React from "react";
import Articles from "../../components/Articles";
import Query from "../../components/Query";
import PRODUCTS_QUERY from "../../queries/products/products";

const Products = () => {
  return (
    <div>
      <div className="uk-section">
        <div className="uk-container uk-container-large">
          <Query query={PRODUCTS_QUERY}>
            {({ data: { products } }) => {
            
            }}
          </Query>
        </div>
      </div>
    </div>
  );
};

export default Products;