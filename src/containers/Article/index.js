import React from 'react'
import { useParams } from 'react-router'
import Query from '../../components/Query'
import ReactMarkdown from 'react-markdown'
import Moment from 'react-moment'
import { useHistory } from 'react-router-dom'

import ARTICLE_QUERY from '../../queries/article/article'

const Article = props => {
  let { id } = useParams()
  const history = useHistory()
  return (
    <Query query={ARTICLE_QUERY} id={id}>
      {({ data: { article } }) => {
        const imageUrl =
          process.env.NODE_ENV !== 'development'
            ? article.image.url
            : process.env.REACT_APP_BACKEND_URL + article.image.url
        return (
          <div>
            <div
              id='banner'
              className='uk-height-medium uk-flex uk-flex-center uk-flex-middle uk-background-cover uk-light uk-padding uk-margin'
              data-src={imageUrl}
              data-srcset={imageUrl}
              data-uk-img
            >
              <h1>{article.title}</h1>
            </div>

            <div className='uk-section'>
              <div className='uk-container uk-container-small'>
                <ReactMarkdown source={article.content} />
                <p>
                  <Moment format='MMM Do YYYY'>{article.published_at}</Moment>
                </p>
                <button onClick={() => history.goBack()} class="uk-button uk-button-default">BACK</button>
              </div>
            </div>
          </div>
        )
      }}
    </Query>
  )
}

export default Article
